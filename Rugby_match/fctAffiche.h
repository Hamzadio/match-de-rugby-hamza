﻿/**
* @brief All fonctions 
* @details  Ce fichier contient la definition des fonctions utlisée pour le panneau de rugby
* @author HMZ LRB
* @date  21/12/23
* @version 0.7
* @file fctAffiche.h
*
*/


#pragma once
#include <iostream>
#include <string>
using namespace std;


/**
*
* @fn void afficheMenu()
*
* @brief Cette fonction affiche le menu des touches
*
*
*/
void afficheMenu();



/**
*
* @fn void afficheScore(int ptsEquipeA, int ptsEquipeB, int taille)
*
* @brief Incremente le score
* @details Cette fonction permet de ajouter un nombre de points en fonction des touches saisies, avec la taille choisi
*
*
* @param[in] TouchePoints La touche qu'ont choisi
* @param[in] taille la taille qu'ont choisi
*
*/
void afficheScore(int ptsEquipeA, int ptsEquipeB, int taille);



/**
*
* @fn void afficheLigneComplete()
*
* @brief Cette fonction affiche une ligne avec tout les éléments
* @param[in] taille La taille de la ligne complete
*
*/
void afficheLigneComplete(int taille);



/**
*
* @fn void afficheLigneVide()
*
* @brief Cette fonction affiche une ligne vide
* 
* @param[in] taille La taille de la ligne vide
*
*/
void afficheLigneVide(int taille);




/**
*
* @fn void afficheNomEquipe(string pNomA, string pNomB, int taille)
*
* @brief affiche les noms des equipes
* @details Cette fonction affiche une ligne avec le nom des equipes et la taille choisi
* 
* @param[in] pNomA nom de l'equipe A(local)
* @param[in] pNomB nom de l'equipe B(exeterne)
* @param[in] taille La taille de la ligne 
*
*/
void afficheNomEquipe(string pNomA, string pNomB, int taille);

/**
*
* @fn void Jump()
*
* @brief Permet de sauter une ligne
*
*/
void Jump();

/**
*
* @fn void PointsEquipes(char TouchePoints, int taille)
*
* @brief ajoute les points et affiche le score
* @details Cette fonction permet de ajouter un nombre de points en fonction des touches saisies et afficher le score
* 
* 
* @param[in] TouchePoints La touche qu'ont choisi
* @param[in] taille La taille qu'ont choisi
*
*/
void PointsEquipes(char TouchePoints, int taille);
