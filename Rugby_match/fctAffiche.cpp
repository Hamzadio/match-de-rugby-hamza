/**
* @brief Fonction du Rugby
* @details  Ce fichier contien le corps des fonctions que le programme qui gere le panneau de rugby utilise
* @author HMZ LRB
* @date  21/12/23
* @version 0.6
* @file fctAffiche.cpp
*
*/


#include "fctAffiche.h"

void PointsEquipes(char TouchePoints, int taille) {

	static int ptsEquipeA = 0;

	static int ptsEquipeB = 0; // statique ne remet pas a 0 la variable



	if (TouchePoints == 'v')  {

		cout << "Fin" << endl;
	}

	
	if (TouchePoints == 'a') {

		ptsEquipeA = ptsEquipeA + 5;

	}
	

	if (TouchePoints == 'z') {

		ptsEquipeA = ptsEquipeA + 2;

	}


	if (TouchePoints == 'e') {

		ptsEquipeA = ptsEquipeA + 3;

	}
	

    if (TouchePoints == 'r') {

		ptsEquipeA = ptsEquipeA + 7;

	}


	if (TouchePoints == 'u') {

		ptsEquipeB = ptsEquipeB + 5;

	}


	if (TouchePoints == 'i') {

		ptsEquipeB = ptsEquipeB + 2;

	}


	if (TouchePoints == 'o') {

		ptsEquipeB = ptsEquipeB + 3;

	 }
	

	if (TouchePoints == 'p') {

		ptsEquipeB = ptsEquipeB + 7;

	}

	afficheScore(ptsEquipeA, ptsEquipeB, taille);

}


void Jump()
{
	cout << '\n';
}


void afficheMenu() {

	cout << "Touche  v  pour sortir du programme" << endl;
	Jump();
	cout << "Touche  a  pour : Essai 5 points (Local)" << endl;
	Jump();
	cout << "Touche  u  pour : Essai 5 points (Exterieure)" << endl;
	Jump();
	cout << "Touche  z  pour : Transformation 2 points (Local)" << endl;
	Jump();
	cout << "Touche  i  pour : Transformation 2 points (Exterieure)" << endl;
	Jump();
	cout << "Touche  e  pour : Pénalités ou Drop 3 points (Local)" << endl;
	Jump();
	cout << "Touche  o  pour : Pénalités ou Drop 3 points (Exterieure)" << endl;
	Jump();
	cout << "Touche  r  pour : Essai de pénalités 7 points (Local)" << endl;
	Jump();
	cout << "Touche  p  pour : Essai de pénalités 7 points (Exterieure)" << endl;
	Jump();
	// cout << "touche  h  pour : annulation du dernier score saisie" << endl;

}


void afficheScore(int ptsEquipeA, int ptsEquipeB, int taille) {

	int resultat = taille / 2;

	if ((ptsEquipeA) < int(10)) {
		if (resultat * 2 == taille) {

			cout << "||";
			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << ptsEquipeA;
			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << "|";

		}


		else {

			cout << "||";
			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << ptsEquipeA;
			for (int i = 0; i < ((taille / 4) - 1); i++) { cout << " "; };
			cout << "|";

		}
	}

	else {

		if ((ptsEquipeA) < int(100)) {
			if (resultat * 2 == taille) {

				cout << "||";
				for (int i = 0; i < ((taille / 4) - 3); i++) { cout << " "; };
				cout << ptsEquipeA;
				for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
				cout << "|";

			}


			else {

				cout << "||";
				for (int i = 0; i < ((taille / 4) - 3); i++) { cout << " "; };
				cout << ptsEquipeA;
				for (int i = 0; i < ((taille / 4) - 1); i++) { cout << " "; };
				cout << "|";

			}
		}


	}

if ((ptsEquipeA) >= int(100)) {
		if (resultat * 2 == taille) {

			cout << "||";
			for (int i = 0; i < ((taille / 4) - 4); i++) { cout << " "; };
			cout << ptsEquipeA;
			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << "|";

		}


		else {

			cout << "||";
			for (int i = 0; i < ((taille / 4) - 4); i++) { cout << " "; };
			cout << ptsEquipeA;
			for (int i = 0; i < ((taille / 4) - 1); i++) { cout << " "; };
			cout << "|";

		}
	}


	if ((ptsEquipeB) < int(10)) {
		if (resultat * 2 == taille) {

			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << ptsEquipeB;
			for (int i = 0; i < ((taille / 4) - 1); i++) { cout << " "; };
			cout << "||" << endl;
		}


		else {

			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << ptsEquipeB;
			for (int i = 0; i < ((taille / 4) - 1); i++) { cout << " "; };
			cout << "||" << endl;

		}
	}
	else {
		if ((ptsEquipeB) < int(100)) {
			if (resultat * 2 == taille) {


				for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
				cout << ptsEquipeB;
				for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
				cout << "||" << endl;

			}


			else {


				for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
				cout << ptsEquipeB;
				for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
				cout << "||" << endl;

			}
		}


	}


	if ((ptsEquipeB) >= int(100)) {

		if (resultat * 2 == taille) {


			for (int i = 0; i < ((taille / 4) - 3); i++) { cout << " "; };
			cout << ptsEquipeB;
			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << "||" << endl;

		}


		else {


			for (int i = 0; i < ((taille / 4) - 3); i++) { cout << " "; };
			cout << ptsEquipeB;
			for (int i = 0; i < ((taille / 4) - 2); i++) { cout << " "; };
			cout << "||" << endl;

		}
	}


	




}


void afficheLigneComplete(int taille) {

	int resultat = taille / 2;

	if (resultat * 2 == taille) {

			cout << "||";
		for (int i = 0; i < ((taille / 2) - 3); i++) { cout << "/"; };
			cout << "|";
		for (int i = 0; i < ((taille / 2) - 2); i++) { cout << "/"; };
			cout << "||"<< endl;

		}
	else {

		cout << "||";
		for (int i = 0; i < ((taille/2) - 2); i++) { cout << "/"; };
		cout << "|";
		for (int i = 0; i < ((taille/2) - 2); i++) { cout << "/"; };
		cout << "||" << endl;

	}
	
		
}


void afficheLigneVide(int taille) {

	int resultat = taille / 2;

	if (resultat * 2 == taille) {

		cout << "||";
		for (int i = 0; i < ((taille / 2) - 3); i++) { cout << " "; };
		cout << "|";
		for (int i = 0; i < ((taille / 2) - 2); i++) { cout << " "; };
		cout << "||" << endl;


	}
	else {

		cout << "||";
		for (int i = 0; i < ((taille / 2) - 2); i++) { cout << " "; };
		cout << "|";
		for (int i = 0; i < ((taille / 2) - 2); i++) { cout << " "; };
		cout << "||" << endl;

	}

}


void afficheNomEquipe(string pNomA, string pNomB, int taille) {

	string nomequipeA = pNomA;
	int nombredecaratereA = nomequipeA.size();

	string nomequipeB = pNomB;
	int nombredecaratereB = nomequipeB.size();

	int resultat = (taille / 4);

	if (resultat * 4 == taille) {

		cout << "||";
		for (int i = 0; i < ((taille / 4) - nombredecaratereA + 1); i++) { cout << " "; };
		cout << pNomA;
		for (int i = 0; i < ((taille / 4) - nombredecaratereA + 2); i++) { cout << " "; };
		cout << "|";
		for (int i = 0; i < ((taille / 4) - nombredecaratereB + 2); i++) { cout << " "; };
		cout << pNomB;
		for (int i = 0; i < ((taille / 4) - nombredecaratereB + 2); i++) { cout << " "; };
		cout << "||" << endl;
	}


	else {

		cout << "||";
		for (int i = 0; i < ((taille / 4) - nombredecaratereA - 1 ); i++) { cout << " "; };
		cout << pNomA;
		for (int i = 0; i < ((taille / 4) - nombredecaratereA + 1); i++) { cout << " "; };
		cout << "|";
		for (int i = 0; i < ((taille / 4) - nombredecaratereB + 2); i++) { cout << " "; };
		cout << pNomB;
		for (int i = 0; i < ((taille / 4) - nombredecaratereB); i++) { cout << " "; };
		cout << "||" << endl;

	}


}



