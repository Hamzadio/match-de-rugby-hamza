﻿/**
* @brief Rugby sign
* @details  Ce fichier contien le programme qui va gerer le panneau
* @author HMZ LRB
* @date  21/12/23
* @version 0.7
* @file gestionMatchRugby.cpp
*
*/


#include "fctAffiche.h"
#include "locale.h"

 int main() {

    setlocale(LC_ALL, "fr_FR");

    char touche = '0';
    int taille = 0;
    string NomA = " ";
    string NomB = " ";

    cout << "Saisir la taille du tableau ";
    cin >> taille;
    cout << "Saisir le nom de l'equipe locale ";
    cin >> NomA;
    cout << "Saisir le nom de l'equipe externe ";
    cin >> NomB;


    while (touche != 'v') {
        afficheLigneComplete(taille);
        afficheNomEquipe(NomA, NomB, taille);
        afficheLigneComplete(taille);
        afficheLigneVide(taille);
        PointsEquipes(touche, taille);
        afficheLigneVide(taille);
        afficheLigneVide(taille);
        afficheLigneComplete(taille);
        afficheMenu();
        Jump();
        Jump();

        cout << "Saisir la touche  ";
        cin >> touche;
        Jump();
        Jump();

    }

    return EXIT_SUCCESS;
}